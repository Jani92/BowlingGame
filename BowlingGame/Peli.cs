﻿namespace BowlingGame
{

    public class Peli
    {
        public int[] Heitot = new int[21];
        public int HeitotAtm = 0;



        public int pisteet
        {
            get
            {
                int pisteet = 0;
                int Rolli = 0;
                for (int frame = 0; frame < 10; frame++)
                {
                    if (Kaato(Rolli))
                    {
                        pisteet += KaatoPojot(Rolli);
                        Rolli++;
                    }

                    else if (Paikko(Rolli))
                    {
                        pisteet = PaikkoPojot(pisteet, Rolli);
                        Rolli += 2;
                    }
                    else
                    {
                        pisteet = VuoronPojot(pisteet, Rolli);
                        Rolli += 2;
                    }
                }
                return pisteet;
            }
        }

        private int VuoronPojot(int pisteet, int Rolli)
        {
            pisteet += Heitot[Rolli] + Heitot[Rolli + 1];
            return pisteet;
        }

        private int PaikkoPojot(int pisteet, int Rolli)
        {
            pisteet += Heitot[Rolli] + Heitot[Rolli + 1] + Heitot[Rolli + 2];
            return pisteet;
        }

        private bool Paikko(int Rolli)
        {
            return Heitot[Rolli] + Heitot[Rolli + 1] == 10;
        }

        private int KaatoPojot(int Rolli)
        {
            return Heitot[Rolli] + Heitot[Rolli + 1] + Heitot[Rolli + 2];
        }

        private bool Kaato(int Rolli)
        {
            return Heitot[Rolli] == 10;
        }


        public void Heitto(int keilat)
        {
            Heitot[HeitotAtm++] = keilat;
        }


    }
}

