﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingGame;
using NUnit.Framework;

namespace BowlingGameTest
{
    [TestFixture]
    public class TestiPeli
    {
        Peli game;


        [SetUp]
        public void SetUp()
        {
            game = new Peli();
        }



        [Test]
        public void NollaPeli()
        {
            MontaHeittoa(10, 0);
            Assert.That(game.pisteet, Is.EqualTo(0));
        }

        [Test]
        public void PelkkiäKolmosia()
        {
            MontaHeittoa(10, 3);
            Assert.That(game.pisteet, Is.EqualTo(30));
        }

        [Test]
        public void KaatoMahdollinen()
        {
            game.Heitto(10);
            game.Heitto(8);
            game.Heitto(7);
            MontaHeittoa(16, 0);
            Assert.That(game.pisteet, Is.EqualTo(40));
        }

        [Test]
        public void ToinenKaatoTesti()
        {
            game.Heitto(10);
            game.Heitto(5);           
            game.Heitto(10);
            game.Heitto(2);
            game.Heitto(1);
            Assert.That(game.pisteet, Is.EqualTo(43));
        }
      

       [Test]
        public void PaikkoMahdollinen()
        {
            game.Heitto(5);
            game.Heitto(5);
            game.Heitto(3);
            MontaHeittoa(17, 0);
            Assert.That(game.pisteet, Is.EqualTo(16));
        }

        [Test]
        public void ToinenPaikkoTesti()
        {
            game.Heitto(3);
            game.Heitto(7);
            game.Heitto(5);
            game.Heitto(2);
            game.Heitto(4);
            MontaHeittoa(15, 0);
            Assert.That(game.pisteet, Is.EqualTo(26));
        }

        [Test]

        public void TäydellinenPeli()
        {
            MontaHeittoa(12, 10);
            Assert.That(game.pisteet, Is.EqualTo(300));
        }

        [Test]
        public void Peli()
        {
            game.Heitto(10);
            game.Heitto(2);
            game.Heitto(7);
            game.Heitto(8);
            game.Heitto(1);
            game.Heitto(2);
            game.Heitto(8);
            game.Heitto(4);
            game.Heitto(6);
            game.Heitto(10);
            game.Heitto(2);
            game.Heitto(5);
            game.Heitto(5);
            game.Heitto(4);
            game.Heitto(9);
            game.Heitto(1);
            game.Heitto(10);
            game.Heitto(5);
            game.Heitto(4);


            Assert.That(game.pisteet, Is.EqualTo(143));

        }


        public void MontaHeittoa(int heitot, int keilat)
        {
            for (int i = 0; i < heitot; i++)
            {
                game.Heitto(keilat);
            }
        }

    }
}
